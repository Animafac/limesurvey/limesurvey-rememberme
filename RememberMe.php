<?php
/**
 * RememberMe class
 */
use Birke\Rememberme\Authenticator;
use LSRememberMe\Storage;

require_once __DIR__.'/vendor/autoload.php';

/**
 * Main plugin class
 */
class RememberMe extends AuthPluginBase
{
    /**
     * Plugin description
     * @var string
     */
    static protected $description = 'Use a cookie to remember logged-in users';

    /**
     * Plugin name
     * @var string
     */
    static protected $name = 'RememberMe';

    /**
     * Did the user just login with a cookie
     * @var boolean
     */
    private $loggedInWithCookie = false;

    /**
     * RememberMe Authenticator instance
     * @var Authenticator
     */
    private $rememberMe;

    /**
     * Initialize plugin
     * @return void
     */
    public function init()
    {
        $this->rememberMe = new Authenticator(new Storage(['plugin'=>$this, 'api'=>$this->api]));
        $this->subscribe('beforeActivate');
        $this->subscribe('afterSuccessfulLogin');
        $this->subscribe('beforeLogin');
        $this->subscribe('newUserSession');
        $this->subscribe('afterLogout');
    }

    /**
     * Create the required SQL tables before activating the plugin
     * @return void
     */
    public function beforeActivate()
    {
        if (!$this->api->tableExists($this, 'tokens')) {
            $this->api->createTable($this, 'tokens', array('credential'=>'integer',
                'token'=>'string',
                'persistentToken'=>'string',
                'expires'=>'timestamp'));
        }
    }

    /**
     * Hijack the login process if we found a valid cookie
     * @return void
     */
    public function beforeLogin()
    {
        if ($this->rememberMe->cookieIsValid()) {
            $cookieValues = explode("|", $_COOKIE[$this->rememberMe->getCookieName()], 3);
            $attributes = $this->api->getUser($cookieValues[0])->getAttributes();
            $this->setUsername($attributes['users_name']);
            $this->setAuthPlugin();
        }
    }

    /**
     * Check if the login cookie is valid
     * @return void
     */
    public function newUserSession()
    {
        if ($userId = $this->rememberMe->login()) {
            $this->setAuthSuccess($this->api->getUser($userId));
            $this->loggedInWithCookie = true;
        }
    }

    /**
     * Create a new login cookie (if the user used another login method)
     * @return void
     */
    public function afterSuccessfulLogin()
    {
        if (!$this->loggedInWithCookie) {
            $userId = $this->api->getCurrentUser()->uid;
            $this->rememberMe->createCookie($userId);
        }
    }

    /**
     * Destroy the cookie after logout
     * @return void
     */
    public function afterLogout()
    {
        $this->rememberMe->clearCookie();
    }
}
