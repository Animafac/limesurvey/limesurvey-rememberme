# LimeSurvey RememberMe plugin

LimeSurvey plugin that adds a cookie to remember logged-in users

## Setup

You need to use [Composer](https://getcomposer.org/) to install the dependencies:

```bash
composer install --no-dev
```

## Usage

A cookie is automatically set when users connect.

## Install

You need to put the plugin in the `plugins/` folder of your LimeSurvey instance and then enable it in the LimeSurvey admin.

## Grunt tasks

[Grunt](https://gruntjs.com/) can be used to run some automated tasks defined in `Gruntfile.js`.

Your first need to install JavaScript dependencies with [Yarn](https://yarnpkg.com/):

```bash
yarn install
composer install
```

### Lint

You can check that the JavaScript, JSON and PHP files are formatted correctly:

```bash
grunt lint
```

### Tests

You can run [PHPUnit](https://phpunit.de/) tests:

```bash
grunt test
```

### Documentation

[phpDocumentor](https://phpdoc.org/) can be used to generate the code documentation:

```bash
grunt doc
```

## CI

[Gitlab CI](https://docs.gitlab.com/ee/ci/) is used to run the tests automatically after each commit.
