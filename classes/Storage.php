<?php
/**
 * Storage class
 */
namespace LSRememberMe;

use Birke\Rememberme\Storage\DB;
use LimeSurvey\PluginManager\LimesurveyApi;
use RememberMe;

/**
 * Class used to store tokens in the database
 *
 * @see https://github.com/gbirke/rememberme/blob/1.0.5/src/Rememberme/Storage/StorageInterface.php
 */
class Storage extends DB
{
    /**
     * LimeSurvey API
     * @var LimesurveyApi
     */
    private $api;

    /**
     * Plugin instance
     * @var RememberMe
     */
    private $plugin;

    /**
     * Find if a triplet exist in the database
     * @param  string $credential      User ID
     * @param  string $token           One-time token
     * @param  string $persistentToken Persistent token
     * @return int
     */
    public function findTriplet($credential, $token, $persistentToken)
    {
        $table = $this->api->getTable($this->plugin, 'tokens');
        $triplet = $table->findAllAsArray(
            'credential = ? AND token = ? AND persistentToken = ?',
            [$credential, $token, $persistentToken]
        );
        if (!empty($triplet)) {
            return self::TRIPLET_FOUND;
        }
    }

    /**
     * Set the LimeSurvey API instance
     * @param LimesurveyApi $api LimeSurvey API
     */
    public function setApi(LimesurveyApi $api)
    {
        $this->api = $api;
    }

    /**
     * Set the plugin instance
     * @param RememberMe $plugin Plugin instance
     */
    public function setPlugin(RememberMe $plugin)
    {
        $this->plugin = $plugin;
    }

    /**
     * Store a triplet in the database
     * @param  string  $credential      User ID
     * @param  string  $token           One-time token
     * @param  string  $persistentToken Persistent token
     * @param  integer $expire          Expiration timestamp
     * @return void
     */
    public function storeTriplet($credential, $token, $persistentToken, $expire = 0)
    {
        $triplet = $this->api->newModel($this->plugin, 'tokens');
        $triplet->credential = $credential;
        $triplet->token = $token;
        $triplet->persistentToken = $persistentToken;
        $triplet->expires = date('Y-m-d H:i:s', $expire);
        $triplet->save();
    }

    /**
     * Replace a triplet in the database
     * @param  string  $credential      User ID
     * @param  string  $token           One-time token
     * @param  string  $persistentToken Persistent token
     * @param  integer $expire          Expiration timestamp
     * @return void
     */
    public function replaceTriplet($credential, $token, $persistentToken, $expire = 0)
    {
        $this->cleanTriplet($credential, $persistentToken);
        $this->storeTriplet($credential, $token, $persistentToken, $expire);
    }

    /**
     * Remove a triplet from the database
     * @param  string  $credential      User ID
     * @param  string  $persistentToken Persistent token
     * @return void
     */
    public function cleanTriplet($credential, $persistentToken)
    {
        $table = $this->api->getTable($this->plugin, 'tokens');
        $table->deleteAllByAttributes(['credential'=>$credential, 'persistentToken'=>$persistentToken]);
    }

    /**
     * Remove all triplets for a specific user
     * @param  string $credential User ID
     * @return void
     */
    public function cleanAllTriplets($credential)
    {
        $table = $this->api->getTable($this->plugin, 'tokens');
        $table->deleteAllByAttributes(['credential'=>$credential]);
    }
}
