<?php
/**
 * StorageTest class
 */

namespace LSRememberMe\Test;

use LSRememberMe\Storage;
use RememberMe;
use Mockery;
use LimeSurvey\PluginManager\LimesurveyApi;

/**
 * Abstract class used to create mock classes
 */
abstract class BaseTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Create mock classes
     */
    protected function setUp()
    {
        Mockery::mock('overload:User');
        Mockery::mock('overload:AuthPluginBase')
            ->shouldReceive('subscribe');
        $currentUser = new \User();
        $currentUser->uid = 'foo';
        Mockery::mock('overload:PluginDynamic')
            ->shouldReceive('findAllAsArray')
            ->andReturn(null, 'foo')
            ->shouldReceive('save')
            ->shouldReceive('deleteAllByAttributes');
        Mockery::mock('overload:'.LimesurveyApi::class)
            ->shouldReceive('tableExists')
            ->shouldReceive('createTable')
            ->shouldReceive('getCurrentUser')
            ->andReturn($currentUser)
            ->shouldReceive('getTable')
            ->andReturn(new \PluginDynamic())
            ->shouldReceive('newModel')
            ->andReturn(new \PluginDynamic());
    }

    /**
     * Unset mocks
     * @return void
     */
    protected function tearDown()
    {
        Mockery::close();
    }
}
