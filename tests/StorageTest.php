<?php
/**
 * StorageTest class
 */

namespace LSRememberMe\Test;

use LSRememberMe\Storage;
use RememberMe;
use LimeSurvey\PluginManager\LimesurveyApi;

/**
 * Unit tests for the Storage class
 */
class StorageTest extends BaseTest
{

    /**
     * Create objects used by all tests
     */
    protected function setUp()
    {
        parent::setUp();

        $plugin = new RememberMe();
        $this->storage = new Storage($plugin);
        $this->storage->setApi(new LimesurveyApi());
        $this->storage->setPlugin($plugin);
    }

    /**
     * Test the findTriplet() function
     * @return void
     */
    public function testFindTriplet()
    {
        $this->assertNull($this->storage->findTriplet('foo', 'bar', 'baz'));
        $this->assertEquals(Storage::TRIPLET_FOUND, $this->storage->findTriplet('foo', 'bar', 'baz'));
    }

    /**
     * Test the storeTriplet() function
     * @return void
     */
    public function testStoreTriplet()
    {
        $this->assertNull($this->storage->storeTriplet('foo', 'bar', 'baz'));
    }

    /**
     * Test the replaceTriplet() function
     * @return void
     */
    public function testReplaceTriplet()
    {
        $this->assertNull($this->storage->replaceTriplet('foo', 'bar', 'baz'));
    }

    /**
     * Test the cleanTriplet() function
     * @return void
     */
    public function testCleanTriplet()
    {
        $this->assertNull($this->storage->cleanTriplet('foo', 'bar', 'baz'));
    }

    /**
     * Test the cleanAllTriplets() function
     * @return void
     */
    public function testCleanAllTriplets()
    {
        $this->assertNull($this->storage->cleanAllTriplets('foo', 'bar', 'baz'));
    }
}
