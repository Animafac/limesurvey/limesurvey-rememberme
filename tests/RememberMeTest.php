<?php
/**
 * RememberMeTest class
 */

namespace LSRememberMe\Test;

use RememberMe;
use LimeSurvey\PluginManager\LimesurveyApi;

/**
 * Unit tests for the RememberMe class
 */
class RememberMeTest extends BaseTest
{

    /**
     * Create objects used by all tests
     */
    protected function setUp()
    {
        parent::setUp();

        $this->plugin = new RememberMe();
        $this->plugin->api = new LimesurveyApi();
        $this->plugin->init();
    }

    /**
     * Test the beforeActivate() function
     * @return void
     */
    public function testBeforeActivate()
    {
        $this->assertNull($this->plugin->beforeActivate());
    }

    /**
     * Test the beforeLogin() function
     * @return void
     */
    public function testBeforeLogin()
    {
        $this->assertNull($this->plugin->beforeLogin());
    }

    /**
     * Test the newUserSession() function
     * @return void
     */
    public function testNewUserSession()
    {
        $this->assertNull($this->plugin->newUserSession());
    }

    /**
     * Test the afterSuccessfulLogin() function
     * @return void
     */
    public function testAfterSuccessfulLogin()
    {
        $this->markTestIncomplete('We need a way to mock HTTP headers response.');
    }

    /**
     * Test the afterLogout() function
     * @return void
     */
    public function testAfterLogout()
    {
        $this->assertNull($this->plugin->afterLogout());
    }
}
